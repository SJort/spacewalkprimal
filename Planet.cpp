//
// Created by this on 2/12/19.
//

#include <iostream>
#include "Planet.h"

Planet::Planet() {

}

Planet::Planet(int planetNumber) {
    this->planetNumber = planetNumber;
}

bool Planet::addShip(Ship *shipToAdd) {
    if(shipToAdd == nullptr){
        return false;
    }

    if(isBlackHole){
        std::cout<<shipToAdd->owner->name<<"'s ship was sucked into the ~V O I D~"<<std::endl;
        shipToAdd->owner->removeShip();
        delete shipToAdd;
        shipToAdd = nullptr;
        return true;
    }
    std::cout<<"Ship with owner "<<shipToAdd->owner->name<<" added to planet "<<planetNumber<<endl;
    ships.push_back(shipToAdd);
    return true;
}

vector<Ship *> Planet::getShipsWithSize(int size) {
    vector<Ship *> result;
    for (Ship *ship:ships) {
        if (ship== nullptr) {
            continue;
        }
        if (ship->size == size) {
            result.push_back(ship);
        }
    }
    return result;
}
