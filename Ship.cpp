//
// Created by this on 2/12/19.
//

#include "Ship.h"

Ship::Ship(){

}

Ship::Ship(Player *owner, int size){
    this->owner = owner;
    this->size = size;
    switch(size){
        case 0:{
            this->representation = "8=D";
            break;
        }
        case 1:{
            this->representation = "8==D";
            break;
        }
        case 2:{
            this->representation = "8===D";
            break;
        }
    }
}