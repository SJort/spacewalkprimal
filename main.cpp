#include "Game.h"
#include "Ship.h"
#include "Planet.h"
#include "Player.h"

#include <iostream>
#include <vector>
#include <cstdlib>

#define DEBUG false

using namespace std;

const int amountOfPlanets = 14;

Player *player0;
Player *player1;
Player *currentPlayer;
int currentPlayerNumber = 0;
int amountOfPlayers = 2;
vector<Planet *> planets;
vector<Player *> players;


void initialize() {
    //board
    for (int i = 0; i < amountOfPlanets; i++) {
        planets.push_back(new Planet(i));
    }
    planets.at(3)->isBlackHole = true;
    planets.at(10)->isBlackHole = true;

    if(DEBUG){
        //test players
//        player0 = new Player();
//        player1 = new Player();
//        player1->isComputer = true;
//        currentPlayer = player0;
//        players.push_back(player0);
//        players.push_back(player1);
//
//        //test ships
//        Ship *ship = new Ship(player0, 1);
//        player0->addShip(ship);
//        planets.at(9)->addShip(ship);
//        ship = new Ship(player1, 2);
//        player1->addShip(ship);
//        planets.at(9)->addShip(ship);
//        ship = new Ship(player0, 0);
//        planets.at(0)->addShip(ship);
//        player0->addShip(ship);
    }

}

void getPlayerDetails(Player *player) {
    cout << "Player " << player->name << ": enter your name."<<endl;
    cout<<"Name: ";
    cin >> player->name;
    cout << "Is computer? 'yes'/'no'";
    string isComputer;
    cin >> isComputer;
    if (isComputer.compare("yes") == 0) {
        player->isComputer = true;
        cout<<player->name<<" is now a computer."<<endl;
    }
}


void startScreen() {
    player0 = new Player("0");
    players.push_back(player0);
    player1 = new Player("1");
    players.push_back(player1);

    for(Player *player :players){
        getPlayerDetails(player);
    }
    currentPlayer = player0;
}

//prints the current ships for each planet
void printBoard() {
    cout << "B O A R D   S T A R T" << endl;
    //temporary ship holder array for checking if a planet contains any
    vector<Ship *> player0Ships;
    vector<Ship *> player1Ships;

    for (int i = 0; i < amountOfPlanets; i++) {
        if (planets.at(i)->isBlackHole) {
            cout << "~~~V O I D~~~" << endl;
        } else {
            cout << "---Planet " << i << "---" << endl;
        }

        //gather the ships from the planet
        for (Ship *ship : planets.at(i)->ships) {
            if (ship != NULL) {
                if (ship->owner == players.at(0)) {
                    player0Ships.push_back(ship);
                } else if (ship->owner == players.at(1)) {
                    player1Ships.push_back(ship);
                }
            }
        }


        //print player 0's ships if any
        if (player0Ships.size() > 0) {
            cout << player0->name << "'s ships: ";
            for (Ship *ship : player0Ships) {
                if (ship != NULL) {
                    //check memory addres of the ship is the same
                    if (ship->owner == player0) {
                        cout << ship->representation << " ";
                    }
                }
            }
            cout << endl;
            player0Ships.clear();
        }

        //print player 1's ships if any
        if (player1Ships.size() > 0) {
            cout << player1->name << "'s ships: ";
            for (Ship *ship : player1Ships) {
                if (ship != NULL) {
                    //check memory addres of the ship is the same
                    if (ship->owner == player1) {
                        cout << ship->representation << " ";
                    }
                }
            }
            cout << endl;
            player1Ships.clear();

        }
        if (planets.at(i)->isBlackHole) {
            cout << "~~~V O I D~~~" << endl;
        } else {
            cout << "---Planet " << i << "---" << endl;
        }
        cout << endl;
    }
    cout << "B O A R D   E N D" << endl;
}

string getStringForShipSize(int size) {
    switch (size) {
        case 0: {
            return "small";
        }
        case 1: {
            return "medium";
        }
        case 2: {
            return "large";
        }
    }
    return "ShipSizeOutOfBounds";
}

bool placeShip(int planetNumber, int shipSize) {
    if(planetNumber<0 || planetNumber > amountOfPlanets){
        cout<<"Planet number "<<planetNumber<<" is out of bounds."<<endl;
        return false;
    }

    Planet *planet = planets.at(planetNumber);
    if(planet->isBlackHole){
        cout<<"Planet "<<planetNumber<<" is a black hole! Try again."<<endl;
        return false;
    }
    for (Ship *ship : planet->ships) {
        if (ship != NULL) {
            if (ship->size == shipSize) {
                cout << "Planet " << planetNumber << " is already occupied with a " << getStringForShipSize(shipSize)
                     << " ship. Try again." << endl;
                return false;
            }
        }
    }
    Ship *ship = new Ship(currentPlayer, shipSize);
    planet->addShip(ship);
    currentPlayer->addShip(ship);
    return true;
}

void setupPhase() {
    int planetNumber;
    //for each ship size(2, 1, 0);
    for (int x = 2; x > -1; x--) {
        //for each ship of that size(3x)
        for (int y = 3; y > 0; y--) {
            //for each player(2x)
            for (Player *player : players) {
                //ask where they want to place a ship
                if (player != NULL) {
                    currentPlayer = player;
                    printBoard();
                    do {
                        cout << currentPlayer->name << ": enter planet number to place a " << getStringForShipSize(x)
                             << " ship on." << endl;
                        cout << "Planet number: ";
                        if (currentPlayer->isComputer) {
                            planetNumber = rand() % amountOfPlanets;
                            cout << planetNumber << endl;
                            cout << "Computer wants to place a ship on planet " << planetNumber << "!" << endl;
                        } else {
                            cin >> planetNumber;
                        }
                    } while (!placeShip(planetNumber, x));

                }
            }
            if(DEBUG){
                currentPlayer = player0;
                return;
            }
        }
    }
}

bool evacuatePlanet(int planetNumber) {
    //check planet number bounds
    if (planetNumber < 0 || planetNumber > amountOfPlanets) {
        cout << "Planet number " << planetNumber << " is out of bounds.";
        return false;
    }

    //check if planet contains any ship of current player
    bool found;
    for (Ship *ship : planets.at(planetNumber)->ships) {
        if (ship == NULL) {
            continue;
        }

        if (ship->owner == currentPlayer) {
            found = true;
            break;
        }
    }
    if (!found) {
        cout << currentPlayer->name << ": planet " << planetNumber << " doesn't contain any of your ships. Try again."
             << endl;
        return false;
    }

    cout << "Evacuating planet " << planetNumber << "..." << endl;

    Planet *planet = planets.at(planetNumber);
    int planetToMoveTo;
    int planetsToMove = 1;

    //for each shipsize
    for (int x = 2; x > -1; x--) {
        vector<Ship *> ships = planet->getShipsWithSize(x);
        for (int i = 0; i < ships.size(); i++) {
            if (ships.at(i) == NULL) {
                continue;
            }
            planetToMoveTo = planetNumber + planetsToMove;
            if (planetToMoveTo >= amountOfPlanets) {
                planetToMoveTo = planetToMoveTo - amountOfPlanets;
            }
            Planet *temp = planets.at(planetToMoveTo);
            temp->addShip(ships.at(i));
            planetsToMove++;

        }
    }
    //remove all ships from the evacuated planet
    planet->ships.clear();
    cout << "Planet " << planetNumber << " evacuated." << endl;
    return true;
}

void setNextPlayer() {
    if (currentPlayerNumber == amountOfPlayers - 1) {
        currentPlayerNumber = 0;
    } else {
        currentPlayerNumber++;
    }
    currentPlayer = players.at(currentPlayerNumber);
    cout<<currentPlayer->name<<": it's your turn!"<<endl;
}

bool doPlayersHaveShips() {
    for (Player *player : players) {
        if (player->amountOfShips < 1) {
            return false;
        }
    }
    return true;
}

void gamePhase() {
    int planetNumber;
    while (doPlayersHaveShips()) {
        printBoard();
        do {
            cout << currentPlayer->name << ": enter planet number to evacuate." << endl;
            cout << "Planet number: ";
            if (currentPlayer->isComputer) {
                planetNumber = rand() % amountOfPlanets;
                cout << planetNumber << endl;
                cout << "Computer wants to evacuate planet " << planetNumber << "!" << endl;
            } else {
                cin >> planetNumber;
            }


        } while (!evacuatePlanet(planetNumber));

        setNextPlayer();
    }
}

void endPhase() {
    cout << "Game over!" << endl;
    for (Player *player : players) {
        if (player->amountOfShips < 1) {
            cout << player->name << " lost the game!" << endl;
            return;
        }
    }
}

int main() {
    cout << "Started S P A C E W A L K" << endl;

    cout << "I N I T I A L I Z E" << endl;
    initialize();
    cout << "S T A R T S C R E E N" << endl;
    startScreen();
    cout << "S E T U P" << endl;
    setupPhase();
    cout << "G A M E" << endl;
    gamePhase();
    endPhase();


    cout << "Spacewalk finished running." << endl;
    return 0;
}