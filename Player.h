//
// Created by this on 2/12/19.
//

#ifndef SPACEWALKPRIMAL_PLAYER_H
#define SPACEWALKPRIMAL_PLAYER_H

#include <string>
#include <vector>
#include "Ship.h"

using namespace std;

//forward declaration to avoid include loop
class Ship;

class Player{
public:
    bool isComputer;
    string name;
    vector<Ship *> ships;
    int amountOfShips;
    Player();
    Player(string name);
    bool hasShips();
    void addShip(Ship*ship);
    void removeShip();
    void removeShipPointerAt(int index);

};


#endif //SPACEWALKPRIMAL_PLAYER_H
