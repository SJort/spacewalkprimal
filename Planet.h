//
// Created by this on 2/12/19.
//

#ifndef SPACEWALKPRIMAL_PLANET_H
#define SPACEWALKPRIMAL_PLANET_H

#include <vector>
#include "Ship.h"

class Planet{
public:
    bool isBlackHole;
    int planetNumber;
    std::vector<Ship*> ships;
    Planet();
    Planet(int planetNumber);
    bool removeShipAtIndex(int index);
    bool addShip(Ship*ship);
    bool areShipsSorted();
    void sortShips();
    vector<Ship*> getShipsWithSize(int size);
};


#endif //SPACEWALKPRIMAL_PLANET_H
