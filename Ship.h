//
// Created by this on 2/12/19.
//

#ifndef SPACEWALKPRIMAL_SHIP_H
#define SPACEWALKPRIMAL_SHIP_H

#include <string>
#include "Player.h"

using namespace std;

class Player; //forward declaration to avoid include loop

class Ship{
public:

    string representation;
    Player *owner;
    int size;
    Ship();
    Ship(Player *owner, int size);
};


#endif //SPACEWALKPRIMAL_SHIP_H
