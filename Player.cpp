//
// Created by this on 2/12/19.
//

#include "Player.h"

Player::Player(){
    this->name = "NoName";
}

Player::Player(string name){
    this->name = name;
}

bool Player::hasShips() {
    for(Ship *ship : ships){
        //if there exists a ship
        if(ship != nullptr){
            return true;
        }
    }
    return false;
    //return amountOfShips > 0;
}

void Player::removeShipPointerAt(int index) {
    //vec.erase(vec.begin() + 1);
    //ships.erase(ships.begin() + index);
}

void Player::addShip(Ship *ship) {
    ships.push_back(ship);
    amountOfShips++;
}

void Player::removeShip() {
    amountOfShips--;
}